 (function() {
  var app = angular.module('public-module', [])
      .controller('RegisterController',['$http', '$state', function($http, $state) {
            this.userCredentials = {};
            this.registerUser = function () {
                console.log(this.userCredentials);
                $http({
                    method: 'POST',
                    url: '/ajax/register/',
                    data: $.param(this.userCredentials),
                    responseType:'json',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded' }
                    //headers: { 'Content-type': 'application/json' }
                }).success(function(data){
                    if(data.success) $state.go("student");
                });
            };
      }])
      .controller('LoginController',['$http', '$state', function($http, $state) {
            this.userCredentials = {};
            this.loginUser = function () {
                console.log(this.userCredentials);
                $http({
                    method: 'POST',
                    url: '/ajax/login/',
                    data: $.param(this.userCredentials),
                    responseType:'json',
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    //headers: { 'Content-type': 'application/json' }
                }).success(function(data){
                    if(data.success) $state.go("student");
                });
            }
      }])
      .config(function($stateProvider, $urlRouterProvider) {
          $urlRouterProvider.otherwise("/");
          $stateProvider
                .state('public', {
                    url: "/",
                    views: {
                        '@' : {templateUrl: 'modules/public/index/'},
                        'content@public': {templateUrl: "modules/public/main/"}
                    }
                })
                .state('login', {
                    url: "login",
                    parent: "public",
                    views: {'content@public': {templateUrl: "modules/public/login/"}}
                })
                .state('register', {
                    url: "register",
                    parent: "public",
                    views: {'content@public': {templateUrl: "modules/public/register/"}}
                });
        });
})();