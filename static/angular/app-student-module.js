 (function() {
  var app = angular.module('student-module', [])
      .config(function($stateProvider) {
          $stateProvider
                .state('student', {
                    url: "/student",
                    views: {
                        '@' : {templateUrl: 'modules/student/index/'},
                        'content@student': {
                            templateUrl: "modules/student/menu/groups",
                            controller:['$http', '$state','$scope', function($http, $state,$scope){
                                var store = this;
                                store.groups = {};
                                $http({method:'GET', url:'/student/ajax/course_groups/'}).success(function(data){
                                    if(data.success) {
                                        store.groups = data.response.groups_list;
                                    }
                                    else if(!data.auth) $state.go("public.login");
                                });
                            }],
                            controllerAs: 'sGroupCtrl'
                        },
                        'menu@student':{
                            templateUrl: "modules/layers/top_menu",
                            controller:['$scope','$http', '$interval', function($scope, $http, $interval) {
                                var store = this;
                                store.quote = {'img':false};
                                var getQuote = function(){
                                    $http({method: 'GET', url: '/student/ajax/quote/'}).success(function(data){
                                        if(data.success) store.quote = data.response.quote;
                                    });
                                };
                                var stop = $interval(getQuote, 720000);
                                getQuote();
                                $scope.$on("$destroy", function() {$interval.cancel(stop);})
                            }],
                            controllerAs: 'sMenuCtrl'
                        }
                    }
                })
                .state('courses', {
                    url: "/courses/{groupId:int}",
                    parent:'student',
                    views: {'content@student': {
                        templateUrl: "modules/student/menu/courses/",
                        controller: ['$http', '$state', '$stateParams', function($http, $state, $stateParams){
                            var store = this;
                            store.courses=[];
                            console.log($stateParams);
                            $http({method:'GET', url:'/student/ajax/courses/'+$stateParams.groupId+'/'}).success(function(data){//as param
                                if(data.success) store.courses = data.response.courses_list;
                                else if(!data.auth) $state.go("public.login");
                            });
                        }],
                        controllerAs: 'sCourseCtrl'
                    }}
                })
                .state('lessons', {
                    url: "/lessons/{courseId:int}",
                    parent:'courses',
                    views: {'content@student': {
                        templateUrl: "modules/student/menu/lessons",
                        controller: ['$http', '$state', '$stateParams', function($http, $state, $stateParams){
                            var store = this;
                            store.lessons =[];
                            $http({method:'GET', url:'/student/ajax/lessons/'+$stateParams.courseId+'/'}).success(function(data){
                                console.log(data.response.lessons_list);
                                if(data.success) {
                                    store.lessons = data.response.lessons_list;
                                    for(var i in store.lessons) store.lessons[i].status='opened';
                                }
                                else if(!data.auth) $state.go("public.login");
                            });
                        }],
                        controllerAs: 'sLessonCtrl'
                    }}
                })
                .state('items', {
                    url: "/items/{lessonId:int}",
                    parent:'lessons',
                    views: {'content@student': {
                        templateUrl: "modules/student/menu/items",
                        controller: ['$http', '$state', function($http, $state, $stateParams){
                            var store = this;

                            this.getItemRoute = function(item){return $state.href('student.courses.lessons.items.'+item.type, {'id':item.id}, {absolute: false});};
                            store.items =[
                                {'id': 1, 'name':'Item 1', 'status':'passed', 'type':'page'},
                                {'id': 2, 'name':'Item 2', 'status':'opened', 'type':'page'},
                                {'id': 3, 'name':'Item 3', 'status':'opened', 'type':'pdf'},
                                {'id': 3, 'name':'Item 3', 'status':'opened', 'type':'video'},
                                {'id': 3, 'name':'Item 3', 'status':'opened', 'type':'exam'},
                                {'id': 4, 'name':'Item 3', 'status':'closed', 'type':'page'}
                            ];
                        }],
                        controllerAs: 'sItemsCtrl'
                    }}
                })
                .state('items_page', {
                    url: "/page/{id:int}",
                    parent:'student.courses.lessons.items',
                    views: {'content@student': {
                        templateUrl: "modules/student/items/page",
                        controller: function(){

                        },
                        controllerAs: 'sPageCtrl'
                    }}
                })
        });
})();