(function() {
    var app = angular.module('tipe10', ['ui.router', 'public-module', 'student-module']).config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    }]);
})();
