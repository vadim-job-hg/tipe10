from django.contrib import admin
from tipe10com.models import *
'''
class QuotesAdmin(admin.ModelAdmin):
    list_display = ('id','img','text', 'top','left', 'color', 'size', 'height', 'width', 'sound')

class CourseAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'description', 'lang', 'payment')

class Student2CoursesAdmin(admin.ModelAdmin):
    list_display = ('id', 'course', 'user', 'date_start', 'status')

admin.site.register(Quotes, QuotesAdmin)
admin.site.register(CourseGroups)
admin.site.register(StudentAreaGroups)
admin.site.register(Course, CourseAdmin)
admin.site.register(Course2Group)
admin.site.register(Student2Courses, Student2CoursesAdmin)
admin.site.register(Lesson)
admin.site.register(Lesson2Course)
admin.site.register(Page)
admin.site.register(TipeExam)
admin.site.register(TipeExamText)
admin.site.register(Certificate)
admin.site.register(Item2Lesson)
'''