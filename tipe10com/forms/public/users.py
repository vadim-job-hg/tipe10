from django import forms
from django.contrib.auth.models import User, Group
class LoginForm(forms.Form):
    username = forms.CharField(label='Your name', max_length=100)
    password = forms.CharField(label='Password', max_length=100)


class RegisterForm(forms.Form):
    username = forms.CharField(label='Your name', max_length=100)
    email = forms.CharField(label='eMail', max_length=100)
    password = forms.CharField(label='Password', max_length=100)
    def save_user(self):
        user = User.objects.create_user(self.cleaned_data['username'], self.cleaned_data['email'], self.cleaned_data['password'])
        group = Group.objects.get(name='students')
        user.groups.add(group)
        return user
