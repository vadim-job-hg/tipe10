from functools import wraps
from django.http import JsonResponse

def ajax_login_required(view_func):
    @wraps(view_func)
    def wrapper(obj, request, *args, **kwargs):
        if request.user.is_authenticated():
            return view_func(obj, request, *args, **kwargs)
        obj.json_response['auth'], obj.json_response['success'], obj.json_response['redirect'], obj.json_response['messages']  = False, False, True, ['please login']
        return JsonResponse(obj.json_response)
    return wrapper