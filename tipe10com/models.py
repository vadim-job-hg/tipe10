from django.db import models
from django.contrib.auth.models import User

class QuotesAuthor(models.Model): #todo: not used
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, null=True)
    class Meta:
        db_table = u'quotes_authors'

    def __str__(self):
        return self.name

class Quotes(models.Model):
    id = models.AutoField(primary_key=True)
    img = models.CharField(max_length=765)
    text = models.TextField(blank=True)
    top = models.IntegerField()
    left = models.IntegerField()
    color = models.CharField(max_length=18)
    size = models.IntegerField()
    height = models.IntegerField()
    width = models.IntegerField()
    sound = models.CharField(max_length=255)
    type = models.CharField(max_length=10, choices=(('regular', 'regular'), ('sparse', 'sparse'), ('secret', 'secret')), default='regular')
    author = models.OneToOneField(QuotesAuthor, on_delete=models.SET_NULL, null=True, blank=True)
    lang = models.CharField(max_length=5, choices=(('rus', 'rus'), ('eng', 'eng')), default='rus')
    class Meta:
        db_table = u'quotes'

    def __str__(self):
        return self.text

#improve
class Course(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    description = models.TextField(default='')
    lang = models.CharField(max_length=5, choices=(('rus', 'rus'), ('eng', 'eng')), default=2) # todo: table later
    payment = models.CharField(max_length=10, choices=(('free', 'free'), ('subscr', 'subscr')), null=False, default=1)
    class Meta:
        db_table = u'course'

    def __str__(self):
        return self.name

# lesson and page is the same
class Lesson(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    description = models.TextField(default='')
    class Meta:
        db_table = u'lesson'

    def __str__(self):
        return self.name

# belong to page
class TipeExamText(models.Model):
    id = models.AutoField(primary_key=True)
    text = models.TextField(default='')
    class Meta:
        db_table = u'tipe_exams_text'
    def __str__(self):
        return self.text

class TipeExam(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    description = models.TextField(default='')
    text = models.ForeignKey(TipeExamText, null=True)
    class Meta:
        db_table = u'tipe_exams'

    def __str__(self):
        return self.name


'''
class Page(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    content = models.TextField()
    description = models.TextField(default='')
    class Meta:
        db_table = u'pages'

    def __str__(self):
        return self.name

class Certificate(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    course = models.ForeignKey(Course)
    user = models.ForeignKey(User)
    hash = models.CharField(max_length=255)
    class Meta:
        db_table = u'certificate'

    def __str__(self):
        return self.name

class PathPoints(models.Model):
    id = models.AutoField(primary_key=True)
    course = models.ForeignKey(Course)
    lesson = models.ForeignKey(Lesson)
    class Meta:
        db_table = u'path_points'

class Student2Courses(models.Model):
    id = models.AutoField(primary_key=True)
    course = models.ForeignKey(Course)
    user = models.ForeignKey(User)
    date_start = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=10, choices=(('inprogress', 'inprogress'), ('finished', 'finished'), ('canstart', 'canstart')), default=1)
    class Meta:
        db_table = u'student2courses'

'''
'''
class StudentCourseProgress(models.Model):
    id = models.AutoField(primary_key=True)
    course = models.ForeignKey(Course)
    user = models.ForeignKey(Users)
    current_lesson_position = models.IntegerField()
    class Meta:
        db_table = u'student_course_progress'

class UserExamProgress(models.Model):  # user have to input answer
    id = models.AutoField(primary_key=True)
    question = models.ForeignKey(Question)
    lesson = models.ForeignKey(Lesson)
    user = models.ForeignKey(Users)
    exam = models.ForeignKey(Exam)
    right = models.CharField(max_length=3, default='chk')  # yes/no/chk - or answer not geted
    answered = models.CharField(max_length=3, default='no')

    class Meta:
        db_table = u'user_exam_progress'
'''
