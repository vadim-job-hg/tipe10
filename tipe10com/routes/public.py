from django.conf.urls import url
from django.views.generic import TemplateView
from tipe10com.views.modules import *
from tipe10com.views.public.users import *

urlpatterns = [
    url(r'^ajax/login/$', ajaxLoginView.as_view(), name='log_ajax'),
    url(r'^ajax/register/$', ajaxRegisterView.as_view(), name='reg_ajax'),
    url(r'^modules/(?P<folder>\w{0,20})/(?P<subfolder>\w{0,20})/(?P<template_name>\w{0,50})/$', AngularSubModulesView.as_view(), name='templates'),
    url(r'^modules/(?P<folder>\w{0,20})/(?P<template_name>\w{0,50})/$', AngularModulesView.as_view(), name='modules'),
    url(r'^$', TemplateView.as_view(template_name="modules/index.html"), name='index')
]