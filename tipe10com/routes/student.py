from django.conf.urls import url
from tipe10com.views.student.lists import *
from tipe10com.views.student.quote import getQuote
#from tipe10.views.student.profile import StudentProfile
#from tipe10.views.student.ajax import ajaxExam
#from tipe10.views.student.course import StudentCourses, StudentCourse, StudentCertificate
#from tipe10.views.student.lesson import StudentLesson, StudentLessonItem
#from tipe10.views.student.menu import StudentMenu

urlpatterns = [
    url(r'^ajax/quote/$', getQuote.as_view(), name='quote_ajax'),
    url(r'^ajax/course_groups/$', getCourseGroupsList.as_view(), name='course_groups_ajax'),
    url(r'^ajax/courses/(?P<group_id>\d+)/$', getCoursesList.as_view(), name='courses_ajax'),
    url(r'^ajax/lessons/(?P<course_id>\d+)/$', getLessonsList.as_view(), name='lessons_ajax'),
    #url(r'^/profile/(?P<name>\b(info|achievement)\b)', StudentProfile.as_view()),
    #url(
    #   r'^/lesson/item/examajax/(?P<lessonid>\w{0,50})/(?P<id>\w{0,50})/(?P<name>\w{0,50})/$',
    #   ajaxExam.as_view(), name='student_ajax'),
    #url(r'^/lesson/item/(?P<type>\w{0,50})/(?P<lessonid>\w{0,50})/(?P<id>\w{0,50})/$',
    #   # todo: list of types
    #   StudentLessonItem.as_view(), name='student_lesson'),
    #url(r'^/lesson/(?P<id>\w{0,50})/$', StudentLesson.as_view(), name='student_lesson'),
    #url(r'^/course/certificate/(?P<course_id>\w{0,50})/$',
    #   StudentCertificate.as_view(template_name="student/course/certificate.html"),
    #   name='student_certificate'),
    #url(r'^/course/(?P<id>\w{0,50})/$', StudentCourse.as_view(), name='student_course'),
    #url(r'^/courses/(?P<id>\w{0,50})/$', StudentCourses.as_view(), name='student_courses'),
    #url(r'^', StudentMenu.as_view(), name='student'),
]