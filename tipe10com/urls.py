from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    #url(r'^student/', include('tipe10com.routes.student')),
    url(r'^', include('tipe10com.routes.public'))
]

handler404 = 'tipe10com.views.errors.page404'
handler403 = 'tipe10com.views.errors.page403'
handler500 = 'tipe10com.views.errors.page500'
