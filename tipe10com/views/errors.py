from django.shortcuts import render_to_response


def page404(request):
    return render_to_response('errors/404.html')

def page403(request):
    return render_to_response('errors/403.html')

def page500(request):
    return render_to_response('errors/500.html')