from django.views.generic import TemplateView
class AngularModulesView(TemplateView):
    template_name = ""
    def dispatch(self, request, folder, template_name, *args, **kwargs):
        if template_name:
            self.template_name = "modules/"+folder+"/"+template_name+".html"
        else:
            self.template_name = 'modules/errors/404.html'
        return super(TemplateView, self).dispatch(request, *args, **kwargs)

class AngularSubModulesView(TemplateView):
    template_name = ""
    def dispatch(self, request, folder, subfolder, template_name, *args, **kwargs):
        if template_name:
            self.template_name = "modules/"+folder+"/"+subfolder+"/"+template_name+".html"
        else:
            self.template_name = 'modules/errors/404.html'
        return super(TemplateView, self).dispatch(request, *args, **kwargs)