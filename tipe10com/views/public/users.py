from django.views.generic.edit import View
from django.db import IntegrityError
from tipe10com.helpers.restful import *
from tipe10com.forms.public.users import *
from django.contrib.auth import authenticate, login

class ajaxRegisterView(JSONResponseMixin, AjaxResponseMixin, View):
    json_response = {'success': True, 'messages': []}
    def post(self, request, *args, **kwargs):
        form = RegisterForm(request.POST)
        if form.is_valid():
            try:
                user = form.save_user()
                if user:
                    login(request, user)
                else:
                    self.json_response = {'success': False, 'messages': ['unknown error']}
            except ValueError  as err:
                self.json_response = {'success': False, 'messages': [str(err)]}
            except IntegrityError:
                self.json_response = {'success': False, 'messages': ['username already exist']}
        else:
            self.json_response = {'success': False, 'messages': form.errors }
        return self.render_json_response(self.json_response)

class ajaxLoginView(JSONResponseMixin, AjaxResponseMixin, View):
    json_response = {'success': True, 'messages': []}
    def post(self, request, *args, **kwargs):
        form = LoginForm(request.POST)
        if form.is_valid():
            try:
                user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
                if user is not None:
                    if not (user.is_active):
                        self.json_response = {'success': False, 'messages': ['user is not active']}
                    elif not(user.has_perm('auth.auth_students')):
                        self.json_response = {'success': False, 'messages': ['user has no permission to login student area', list(user.get_group_permissions())]}
                    else:
                        login(request, user)
                else:
                    self.json_response = {'success': False, 'messages': ['wrong username or password']}
            except ValueError  as err:
                self.json_response = {'success': False, 'messages': [err.message]}
        else:
            self.json_response = {'success': False, 'messages': form.errors}
        return self.render_json_response(self.json_response)

