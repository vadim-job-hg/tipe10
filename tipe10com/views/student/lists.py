from django.views.generic import View
from tipe10com.helpers.restful import *
from tipe10com.helpers.login_required import ajax_login_required
from tipe10com.models import CourseGroups

class getCourseGroupsList(JSONResponseMixin, AjaxResponseMixin, View):
    json_response = {'success':True, 'auth':True, 'redirect':False, 'response':{'groups_list':[]}}

    @ajax_login_required
    def get(self, request, *args, **kwargs):
        self.json_response['response']['groups_list'] = self.getData(request)
        return self.render_json_response(self.json_response)

    def getData(self, request):
        return list(CourseGroups.objects.order_by('id').all().values())#todo: to model


class getCoursesList(JSONResponseMixin, AjaxResponseMixin, View):
    json_response = {'success': True, 'auth':True, 'redirect':False, 'response': {'courses_list': []}}
    @ajax_login_required
    def get(self, request, group_id, *args, **kwargs):
        self.json_response['response']['courses_list'] = self.getData(request, group_id)
        return self.render_json_response(self.json_response)

    def getData(self, request, group_id):
        from django.db import connection
        cursor = connection.cursor()
        cursor.execute("""
               SELECT c.id as course_id, c.name as cname, c2g.status, c.payment, s2c.status as s2status
               FROM course2group c2g
               INNER JOIN course c ON c.id = c2g.course_id
               LEFT JOIN student2courses s2c ON c.id = s2c.course_id AND s2c.user_id = '%s'
               WHERE c2g.status IN ('opened') and c2g.group_id = %s """, [request.user.pk, group_id])
        result_list = []
        for row in cursor.fetchall():
            buyed = row[4] if not (row[4] is None) else False
            result_list.append({'id':row[0], 'name':row[1], 'status':row[2], 'payment':row[3], 'sstatus':buyed})
        return result_list


class getLessonsList(JSONResponseMixin, AjaxResponseMixin, View):
    json_response = {'success': True, 'auth':True, 'redirect':False, 'response': {'lessons_list': []}}
    @ajax_login_required
    def get(self, request, course_id, *args, **kwargs):
        self.json_response['response']['lessons_list'] = self.getData(request, course_id)
        return self.render_json_response(self.json_response)

    def getData(self, request, course_id):
        from django.db import connection
        cursor = connection.cursor()
        cursor.execute("""
               SELECT l.id, l.name as lname
               FROM course c
               INNER JOIN student2courses s2c ON c.id = s2c.course_id AND s2c.user_id = '%s'
               INNER JOIN lesson2course l2c ON c.id = l2c.course_id
               INNER JOIN lesson l ON l.id = l2c.lesson_id
               WHERE c.id = %s and c.payment IN ('free', 'canbuy') """, [request.user.pk, course_id])
        result_list = []
        for row in cursor.fetchall():
            result_list.append({'id':row[0], 'name':row[1], 'status':'opened'})
        return result_list

class getItemsList(JSONResponseMixin, AjaxResponseMixin, View):
    json_response = {'success': True, 'auth':True, 'redirect':False, 'response': {'lessons_list': []}}
    def get(self, request, *args, **kwargs):
        return self.render_json_response(self.json_response)