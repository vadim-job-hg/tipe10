from django.views.generic import View
from tipe10com.helpers.restful import *
from tipe10com.helpers.login_required import ajax_login_required
from tipe10com.models import Quotes

class getQuote(JSONResponseMixin, AjaxResponseMixin, View):
    json_response = {'success':True, 'auth':True, 'response':{'quote':{}}}
    @ajax_login_required
    def get(self, request, *args, **kwargs):
        quote = Quotes.objects.values('img','text', 'top','left', 'color', 'size', 'height', 'width', 'sound').order_by('?').first()
        if quote:
            self.json_response['response']['quote'] = quote
        return self.render_json_response(self.json_response)